#### #  ###  # #
#    # ##    # #
###  #   #   ###
#    # #  #  # #
#    #  ##   # #

alias s="ssh"
alias g="git"
alias c="clear"
alias q="exit"

function fish_prompt
	 echo (pwd) '$ '
         end

# EOF