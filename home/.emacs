;; Free Man 6/2/19                                        
;;                                                       
;;       (((((((  (((    (((    (())     ))))))))  ))))))   
;;       ((       ((((  ((((   ((  ))   )))       )))       
;;       (((((((  ((( (( (((   ((()))   )))         ))))   
;;       ((       (((    (((  (((  )))  )))            )))  
;; (( )) (((((((  (((    ((( (((    )))  ))))))))  )))))) 
     

;; Set up a proper melpa repo
(require 'package)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes (quote (wheatgrass)))
 '(display-battery-mode t)
 '(global-display-line-numbers-mode t)
 '(inhibit-startup-screen t)
 '(line-number-mode nil)
 '(package-archives
   (quote
    (("gnu" . "http://elpa.gnu.org/packages/")
     ("melpa-stable" . "http://stable.melpa.org/packages/"))))
 '(package-selected-packages (quote (treemacs ace-window magit helm yasnippet symon)))
 '(tool-bar-mode nil))
(package-initialize)

;; My settings
(require 'symon)
(symon-mode)
(helm-mode)
(menu-bar-mode -1) ; disable menu-bar
(toggle-scroll-bar -1) ; disable scroll-bar
(setq make-backup-files nil) ; stop creating backup~ files
(setq auto-save-default nil) ; stop creating #autosave# files
(setq create-lockfiles nil) ; stop creating .#lock files
(global-visual-line-mode t)

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Input Mono Compressed" :foundry "FBI" :slant normal :weight light :height 120 :width normal)))))

;; Let's make window management easier

(global-set-key (kbd "S-C-<left>") 'shrink-window-horizontally)
(global-set-key (kbd "S-C-<right>") 'enlarge-window-horizontally)
(global-set-key (kbd "S-C-<down>") 'shrink-window)
(global-set-key (kbd "S-C-<up>") 'enlarge-window)
(global-set-key (kbd "M-p") 'other-window)
(global-set-key (kbd "C-<pause>") 'ace-window)
(global-set-key (kbd "M-<print>") 'ace-maximize-window)
(global-set-key (kbd "M-<Scroll_Lock>") 'split-window-right)
(global-set-key (kbd "M-<pause>") 'split-window-below)
(global-set-key (kbd "M-q") 'delete-window)
(windmove-default-keybindings)

;; treemacs shortcut

(global-set-key (kbd "M-<tab>") 'treemacs) 

;; quicksave

(global-set-key (kbd "M-s") 'save-buffer)
(global-set-key (kbd "M-S-s") 'save-some-buffers)

;; Misc

(global-set-key (kbd "C-S-u") 'undo)
(global-set-key (kbd "M-f") 'isearch-forward-word)

;; THE FUNCTION KEYS

(global-set-key (kbd "M-<f9>") 'info-emacs-manual)
(global-set-key (kbd "M-<f10>") 'kill-ring-save)
(global-set-key (kbd "M-<f11>") 'clipboard-yank)
(global-set-key (kbd "C-<f12>") 'kill-emacs)


(defun 2-windows-vertical-to-horizontal ()
  (let ((buffers (mapcar 'window-buffer (window-list))))
    (when (= 2 (length buffers))
      (delete-other-windows)
      (set-window-buffer (split-window-horizontally) (cadr buffers)))))

(add-hook 'emacs-startup-hook '2-windows-vertical-to-horizontal)

;; End of file, more to come.
