--[[
#=====================================================================================
#                               aureola
# Date    : 24/01/2017
# Author  : Erik Dubois at http://www.erikdubois.be
# Version : v1.7.9
# License : Distributed under the terms of GNU GPL version 2 or later
# Documentation : http://erikdubois.be/category/linux/aureola/
#======================================================================================

#======================================================================================
#                              
# Source : Daniel Zet Google+
#
#======================================================================================

]]

conky.config = {

    --Various settings

    background = true,                             -- forked to background
    cpu_avg_samples = 2,
    diskio_avg_samples = 10,
    double_buffer = true,
    if_up_strictness = 'address',
    net_avg_samples = 2,
    no_buffers = true,
    temperature_unit = 'celsius',
    text_buffer_size = 1048,
    update_interval = 3,
       
           
    --Placement

    alignment = 'top_right',
    gap_x = 5,
    gap_y = 5,
    minimum_height = 250,
    minimum_width = 335,
    maximum_width = 335,
       
    --Graphical

    border_inner_margin = 10,                     -- margin between border
    border_outer_margin = 1,                     -- margin between border
    border_width = 0,                             -- border width in pixels
    default_bar_width = 280,
    default_bar_height = 10,
    default_gauge_height = 25,
    default_gauge_width =40,
    default_graph_height = 40,
    default_graph_width = 153,
    default_shade_color = '#000000',
    default_outline_color = '#000000',
    draw_borders = false,                        --draw borders around text
    draw_graph_borders = true,
    draw_shades = false,
    draw_outline = false,
    stippled_borders = 0,
       

    --Textual
   
    extra_newline = false,
    format_human_readable = true,
    font = 'Fixed:size=12:regular',
    max_text_width = 0,
    max_user_text = 16384,
    override_utf8_locale = true,
    short_units = true,
    top_name_width = 21,
    top_name_verbose = false,
    uppercase = false,
    use_spacer = 'none',
    use_xft = true,
    xftalpha = 1,

    --Windows

    own_window = true,
    own_window_argb_value = 190,
    own_window_argb_visual = true,
    own_window_class = 'Conky',
    own_window_colour = '#000000',
    own_window_hints = 'undecorated,below,sticky,skip_taskbar,skip_pager',
    own_window_transparent = no,
    own_window_title = 'system_conky',
    own_window_type = 'override',-- # options are: normal/override/dock/desktop/panel
   

    --Colours

    default_color = '#FFFFFF',                  -- default color and border
    color1 = '#01C830',
    color2 = '#FFFFFF',
    color3 = '#C64CFE',
    color4 = '#F2CE1D',
    color5 = '#FF0000',
    color6 = '#4CB1FE',
    color7 = '#04B404',
    color8 = '#AFADB0',
    color9 = '#19FF9C',
   

    --Signal Colours
    color11 = '#1F7411',  --green
    color12 = '#FFA726',  --orange
    color13 = '#F1544B',  --firebrick
   

    --Lua


};

conky.text = [[
#${color2}+-----------------------------------------------------------+${color2}
${goto 12}${color1}+----System${color2}
${goto 15}
#${goto 15}${goto 38}${color6}${exec whoami}${font}${color1}@${color3}${nodename}${color2}
${goto 15}${goto 38}+-- ${color2}Os${goto 155}${color5}${sysname}${color}
${goto 15}${goto 38}+-- ${color2}Kernel ${goto 155}${color4}${kernel}${color}
${goto 15}${goto 38}+-- ${color2}Machine${goto 155}Dell Optiplex 780${color}
${goto 15}${goto 38}+-- ${color2}WM${goto 155}${color3}Fluxbox${color}
${goto 15}${goto 38}+-- ${color2}Uptime ${goto 155}${color6}${uptime}${font}${color}
${goto 15}
${goto 12}${color1}+----Network ${color}
${goto 15}${goto 40}
${goto 15}${goto 38}+-- ${color2}Up${color}
${goto 15}${goto 40}${goto 60}+-- ${color3}Speed${goto 155}${color4}${upspeedf em0}KB${color}
${goto 15}${goto 40}${goto 60}+-- ${color3}Total${goto 155}${color4}${totalup em0}${color}
${goto 15}${goto 38}+-- ${color2}Down${color}
${goto 15}${goto 60}+-- ${color3}Speed${goto 155}${color4}${downspeedf em0}KB${color}
${goto 15}${goto 60}+-- ${color3}Total${goto 155}${color4}${totaldown em0}${color}
${goto 15}
${goto 12}${color1}+----Memory ${color}
${goto 15}${goto 40}
${goto 15}${goto 38}+-- ${color2}Total${goto 155}${color4}${memmax}${color}
${goto 15}${goto 38}+-- ${color2}In Use${goto 155}${color4}${mem} ${color6}(${memperc}%)${color}
${goto 15}${goto 38}+-- ${color2}Free${goto 155}${color4}${memfree}${color}
${goto 15}${goto 38}+-- ${color2}Up to${goto 155}${color4}${memeasyfree}${color}
${goto 15}
${goto 12}${color1}+----Status${color}
${goto 15}${goto 40}
${goto 15}${goto 38}+-- ${color2}Cpu ${goto 155}${color3}1> ${color6}${cpu cpu0}% ${color2}
${goto 15}${goto 155}${color3}2> ${color6}${cpu cpu1}% ${color2}
${goto 15}${goto 38}+-- ${color2}Temp ${goto 155}${color3}1> ${color6}${exec sysctl -n dev.cpu.0.temperature}${color3} 2> ${color6}${exec sysctl -n dev.cpu.1.temperature}${color}
#${goto 15}${goto 38}+-- ${color2}Battery${goto 155}${color6}${battery}
#${goto 15}${goto 155}${color2}Time Left (H:M) ${color6}${battery_time}${color2}
${goto 15}
${goto 12}${color1}+----Storage ${color2}
${goto 15}${goto 40}
${goto 15}${goto 38}+-- ${color2}HDD${goto 155}${color6}${fs_used /}${color2} / ${color1}${fs_size /}${color2} - ${color3}${fs_used_perc}%
${goto 15}
${goto 15}${goto 38}${color2}Today is ${color1}${time %m.%d.%Y}
${goto 15}
${goto 15}${goto 38}${color3}Stay ${color6}frosty ${color1}jellybean...${color2}
${goto 15}${goto 32}${color9} ______              ${color4}  ____   _____ _____
${goto 15}${goto 32}${color9}|  ____|__ ____  ___ ${color4} |  _ \ / ____|  __ \${color9}        
${goto 15}${goto 32}${color9}| |__ | __V  _ \/ _ \${color4} | |_) | (___ | |  | |
${goto 15}${goto 32}${color9}|  __|| | |  __/  __/${color4} |  _ < \___ \| |  | |
${goto 15}${goto 32}${color9}| |   | | |    |    |${color4} | |_) |____) | |__| |
${goto 15}${goto 32}${color9}|_|   |_|  \___|\___|${color4} |     |      |      |
${goto 15}${goto 32}${color5}+---------------------${color4}|____/|_____/|_____/
${goto 15}
${goto 12}${color6}Important Commands -
${goto 15}
${goto 12}${color1}Mount CD/DVD - ${color4}mount -t cd9660 /dev/cd0 /cdrom
${goto 12}${color1}Mount USB - ${color4}mount_msdosfs /dev/da0p1 /mnt
#${goto 15}${goto 95}  ____   _____ _____
#${goto 15}${goto 95} |  _ \ / ____|  __ \${color2}
#${goto 15}${goto 95} | |_) | (___ | |  | |
#${goto 15}${goto 95} |  _ < \___ \| |  | |
#${goto 15}${goto 95} | |_) |____) | |__| |
#${goto 15}${goto 95} |     |      |      |
#${goto 15}${goto 95} |____/|_____/|_____/
]]
